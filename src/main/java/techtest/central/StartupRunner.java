package techtest.central;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import techtest.central.service.RestClient;
import techtest.central.service.SoapClient;
import techtest.central.service.SocketClient;

@Component
public class StartupRunner implements CommandLineRunner {

	private static Logger logger = LoggerFactory.getLogger(StartupRunner.class);
	Scanner scanner = new Scanner(System.in);

	@Override
	public void run(String... args) throws Exception {

		logger.info("Application started.");

		String option;
		boolean correctOption = false;
		String name;
		String response = null;

		do {
			do {
				System.out.println();
				System.out.println("MAIN MENU");
				System.out.println("=========");
				System.out.println("1. Send message to REST Server");
				System.out.println("2. Send message to SOAP Server");
				System.out.println("3. Send message to Socket Server");
				System.out.println();
				System.out.println("Select an option (1/2/3/q): ");

				option = scanner.nextLine();

				correctOption = "1".equals(option) || "2".equals(option) || "3".equals(option) || "q".equals(option);
				if (!correctOption) {
					System.out.println("Error: Wrong option.");
				}

			} while (!correctOption);

			System.out.println("Enter your name: ");
			name = scanner.nextLine();
			System.out.println("Communicating with REST Server with input " + name + "...");

			switch (option) {
			case "1":
				response = callRestServer(name);
				break;
			case "2":
				response = callSoapServer(name);
				break;
			case "3":
				response = callSocketServer(name);
				break;
			case "q":
			default:
			}

		} while (!"q".equals(option));

		System.out.println("The response is:");
		System.out.println(response);

		scanner.close();

		logger.info("Application shutdown.");
	}

	private String callRestServer(String name) {
		System.out.println("Calling REST Server with input " + name + "...");
		RestClient restClient = new RestClient();
		
		// Insert code here.
		
		return restClient.callRestServer(name);
	}

	private String callSoapServer(String name) {
		System.out.println("Calling with SOAP Server with input " + name + "...");
		SoapClient soapClient = new SoapClient();

		// Insert code here.
		
		return soapClient.callSoapServer(name);
	}

	private String callSocketServer(String name) {
		System.out.println("Calling with Socket Server with input " + name + "...");
		SocketClient socketClient = new SocketClient();

		// Insert code here.
		
		return socketClient.callSocketServer(name);
	}

}
